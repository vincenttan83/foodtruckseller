//
//  FoodLoader.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 12/5/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import UIKit

class FoodLoader {
    //Singleton Pattern
    static let sharedLoader: FoodLoader = FoodLoader()
    
    //Initialize
    private init(){
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = ["X-Parse-Application-Id": "tCUyLTP0oZI3TTfr2MIA2GCkaOP7bFAor44oSD0U", "X-Parse-REST-API-Key": "0g1vSqt0OAhOm3mDUnTj1Hy94mrOSyihSLZNsJf5", "Content-Type": "application/json"]
        self.session = NSURLSession(configuration: config)
        self.baseURL = NSURL(string: "https://api.parse.com/1/")!
    }
    
    // Manage data files
    let fileManager: NSFileManager = NSFileManager.defaultManager()
    
    // Manage loading from server
    let session: NSURLSession
    let baseURL: NSURL
    
    
}

//Load Foods from API
extension FoodLoader{
    func readFoodsFromServer(completionBlock:((foods: [Food], error: NSError?) -> Void)?){
        let url: NSURL = NSURL(string: "classes/Food", relativeToURL: self.baseURL)!
        
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            // Check returned data validity
            // let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            var json: [String : AnyObject]!
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            } catch let jsonError {
                // Return error
                completionBlock?(foods: [], error: jsonError as NSError)
                return
            }

            // Process JSON into Park array
            var foodsFromServer: [Food] = []
            
            if let resultsArray: [ [String : AnyObject] ] = json["results"] as? [ [String : AnyObject] ] {
                for jsonDict in resultsArray {
                    let food: Food = Food(json: jsonDict)!
                    foodsFromServer.append(food)
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completionBlock?(foods: foodsFromServer, error: nil)
            })
        }
        
        loadTask.resume()
    }
    
    func saveFoodOnServer(food: Food, successBlock:((succeeded: Bool, error: NSError?) -> Void)?) {
        if food.objectId == nil {
            createFoodOnServer(food, successBlock: successBlock)
        } else {
            updateFoodOnServer(food, successBlock: successBlock)
        }
    }
    
    func createFoodOnServer(food: Food, successBlock:((succeeded: Bool, error: NSError?) -> Void)?) {
        let url: NSURL = NSURL(string: "classes/Food", relativeToURL: self.baseURL)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        
        let jsonDict: [String : AnyObject] = food.jsonDict()
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions(rawValue: 0))
        
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // Check HTTP Code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            
            // Get new object ID and save to park
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            let json : [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            food.objectId = json["objectId"] as? String
            
            successBlock?(succeeded: true, error: nil)
            NSLog("HTTP \(httpCode): \(jsonString!)")
        }
        
        postTask.resume()
    }

    func updateFoodOnServer(food: Food, successBlock:((succeeded: Bool, error: NSError?) -> Void)?) {
        let url: NSURL = NSURL(string: "classes/Food/\(food.objectId!)", relativeToURL: self.baseURL)!
        
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(food.jsonDict(), options: NSJSONWritingOptions(rawValue: 0))
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            let json : [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            food.objectId = json["objectId"] as? String
            NSLog("HTTP \(httpCode): \(jsonString!)")
        }
        postTask.resume()
    }
    
    func createImageOnServer(image: UIImage, completionBlock:((fileURL: NSURL?, error: NSError?)->Void)?) {
        let url: NSURL = NSURL(string: "files/photo.jpg", relativeToURL: self.baseURL)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("image/jpeg", forHTTPHeaderField: "Content-Type")
        
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        
        let postTask: NSURLSessionDataTask = self.session.uploadTaskWithRequest(urlRequest, fromData: imageData) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // Check HTTP code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            
            // Receive file url and name
            let json : [String : AnyObject]!
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            } catch let jsonError {
                completionBlock?(fileURL: nil, error: jsonError as NSError)
                return
            }
            
            let fileUrlString: String = json["url"] as! String
            let fileURL: NSURL = NSURL(string: fileUrlString)!
            completionBlock?(fileURL: fileURL, error: nil)
            
            NSLog("HTTP \(httpCode): \(json)")
        }
        
        postTask.resume()
    }

    func loadImageTask(imageURL: NSURL, completionBlock:((image: UIImage!, error: NSError?) -> Void)?) -> NSURLSessionDataTask {
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: imageURL)
        urlRequest.cachePolicy = NSURLRequestCachePolicy.ReturnCacheDataElseLoad
        
        let task: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (imageData: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            var image: UIImage! = nil
            if let imageData = imageData {
                image = UIImage(data: imageData)
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completionBlock?(image: image, error: error)
            })
        }
        
        return task
    }

}

// Load Foods from file system
extension FoodLoader {
    func dataFileURL() -> NSURL {
        //NSSearchPathDomainMask.UserDomainMask - specific to user document
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        return filePath
    }
    
    //Manage data files
    func readFoodsFromFile() -> [Food] {
        let foodsData: [NSData] = NSArray(contentsOfURL: self.dataFileURL()) as! [NSData]
        var foods: [Food] = []
        
        for data in foodsData {
            let food: Food = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Food
            foods.append(food)
        }
        return foods
    }
    
    func saveFoodsToFile(foods: [Food]){
        var foodsData: [NSData] = []
        for food in foods {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(food)
            foodsData.append(data)
        }
        
        (foodsData as NSArray).writeToURL(self.dataFileURL(), atomically: true)
        
    }
}