//
//  Food.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/20/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import Foundation
import UIKit

class Food: NSObject, NSCoding {
    
    var objectId: String?
    var name: String
    var price: Double?
    var ratings: [Rating]
    var location: String?
    
    // For newly added photo (on device)
    var photo: UIImage?
    
    // For photos stored on server
    var photoUrl: NSURL?
    var photoFilename: String? {
        get {return self.photoUrl?.pathComponents?.last}
    }
    
    //cause protocol
    func encodeWithCoder(aCoder: NSCoder){
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encodeObject(self.name, forKey: "FoodName")
        keyedArchiver.encodeObject(self.price, forKey: "FoodPrice")
        
        if self.photo != nil {
            let imageData = UIImagePNGRepresentation(self.photo!)
            keyedArchiver.encodeObject(imageData, forKey: "FoodPhotoData")
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        self.init(name: keyedUnarchiver.decodeObjectForKey("FoodName") as! String, price: keyedUnarchiver.decodeObjectForKey("FoodPrice") as? Double)
        
        if let imageData = keyedUnarchiver.decodeObjectForKey("FoodPhotoData") as? NSData {
            self.photo = UIImage(data: imageData)
        }
//        self.price = keyedUnarchiver.decodeObjectForKey("FoodPrice") as? Double
        
        self.ratings = []
    }
    
    init?(name: String, price: Double?){
        self.name = name
        self.price = price
        self.ratings = []
        
        //cause NSObject need to add this line
        super.init()
        
        if name.isEmpty {
            return nil
        }
        
        if price == nil {
            return nil
        }
    }
    
    func averageFoodItemRating() -> Double{
        if ratings.isEmpty {
            return 0.0
        } else {
            var sum : Double = 0
            
            for food in self.ratings {
                if food.rating != nil {
                    
                    sum += Double(food.rating!)
                }
            }
            return sum / Double(ratings.count)
        }
    }
    
    func addRating(ratingValue: Double?, comment: String, dateCreated: NSDate?, dateUpdated: NSDate?, createdBy: String) {
//        self.ratings = [Rating(rating: ratingValue, comment: comment, dateCreated: dateCreated, dateUpdated: dateUpdated, createdBy: createdBy)]
        
        if let rating = Rating(rating: ratingValue, comment: comment, dateCreated: dateCreated, dateUpdated: dateUpdated, createdBy: createdBy) {
            self.ratings.append(rating)
        }
    }

    func sortedRating() -> [Rating] {
        //        self.foods.sort { (firstFood: Food, isOrderedBefore secondFood: Food) -> Bool in
        //            return firstFood.name! < secondFood.name!
        //        }
        return self.ratings.sort({ $0.rating! < $1.rating! })
    }
    
    class func createRandomFood() -> Food {
        
        let dataset: [String] = ["Nasi Lemak", "Mee Goreng", "Roti Canai", "Honey Waffle", "Hot Dog", "Ice-cream", "Beer", "Mineral Water"]
        let index: Int = Int(arc4random_uniform(UInt32(dataset.count)))
        let randomName: String = dataset[index]
        
        let food: Food = Food(name: randomName, price: 15.00)!
        
        return food
    }
}

// JSON API
extension Food {
    convenience init?(json: [String : AnyObject]) {
        // Get values from json dictionary
        let name: String = json["foodName"] as! String
        let price: Double = json["price"] as! Double
        self.init(name: name, price: price)
        
        updateWithJson(json)
    }
    
    func updateWithJson(json: [String : AnyObject]) {
        if let name = json["foodName"] as? String {
            self.name = name
        }
        
        if let price = json["price"] as? Double {
            self.price = price
        }
        
        if let objectId: String = json["objectId"] as? String {
            self.objectId = objectId
        }
        
        if let photoDict: [String : AnyObject] = json["photo"] as? [String : AnyObject] {
            if let photoUrlString: String = photoDict["url"] as? String {
                self.photoUrl = NSURL(string: photoUrlString)
            }
        }
    }
    
    func jsonDict() -> [String : AnyObject] {
        var jsonDict: [String : AnyObject] = ["foodName": self.name, "price": self.price ?? NSNull()]
        
        if let _ = self.photoUrl {
            jsonDict["photo"] = ["name": self.photoFilename!, "__type": "File"]
        }
        
        return jsonDict
    }
}