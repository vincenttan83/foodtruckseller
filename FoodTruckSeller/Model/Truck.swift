//
//  Truck.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/20/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Truck {
    var name: String
    var location: CLLocation?
    var onAir: Bool? = false
    var foods: [Food] = []
    var phone: String?
    var ratings: [Rating] = []
    var truckPicture: UIImage?
    var schedule: String?
    
    init(name: String){
        self.name = name
//        self.location = location
//        self.phone = phone
          self.ratings = []
//        self.truckPicture = truckPicture
    }
    
    
    // see my average rating by user
    func AverageRating() -> Double {
        if ratings.count == 0 {
            return 0.0
        }
        var average: Double = 0.0
        for ratings in self.ratings {
            average += Double(ratings.rating!)
        }
        return average / Double(ratings.count)
    }
    
    // add rating
    func addRating(ratingValue: Double, comment: String, commentor: String) {
        
        if let newRating = Rating(rating: ratingValue, comment: comment, dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: commentor) {
            self.ratings.append(newRating)
        }
        
    }
    
    // add food
    func addFood(name: String, price: Double) {
        
        var newFood = Food (name: name, price: price)
        
        
        
        self.foods.append(newFood!)
    }
    
    
    // start and stop selling
    func checkIn() {
        onAir = true
    }
    
    func checkOut() {
        onAir = false
    }
    
    func sortedFoodByName() -> [Food] {
//        self.foods.sort { (firstFood: Food, isOrderedBefore secondFood: Food) -> Bool in
//            return firstFood.name! < secondFood.name!
//        }
        return self.foods.sort({ $0.name < $1.name })
    }
}
