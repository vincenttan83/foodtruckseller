//
//  Rating.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/20/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import Foundation

class Rating {
    var comment: String?
    var dateCreated: NSDate?
    var dateUpdated: NSDate?
    var createdBy: String?
    var rating: Double?
//        {
//        didSet {
//            if let rating = self.rating {
//                setRatingRangeBetweenOneToFive(rating)
//            }
////            rating = min(max(rating,1),5)
//        }
//    
//    }
    
    init?(rating: Double?, comment: String, dateCreated: NSDate?, dateUpdated: NSDate?, createdBy: String){
        self.rating = rating
        self.comment = comment
        self.dateCreated = dateCreated
        self.createdBy = createdBy
        
        if dateCreated == nil || dateUpdated == nil || rating == nil {
            return nil
        }
        
        if comment.isEmpty || createdBy.isEmpty {
            return nil
        }
        
//        self.rating?.
        if let rating = self.rating {
            setRatingRangeBetweenOneToFive(rating)
        }
    
    }
    
    func setRatingRangeBetweenOneToFive(rating: Double?){
        if rating < 1.0 {
            self.rating = 1.0
        } else if rating > 5.0 {
            self.rating = 5.0
        }
    }
    

}