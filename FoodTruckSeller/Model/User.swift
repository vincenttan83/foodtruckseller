//
//  User.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/20/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import Foundation

class User {
    var id: Int?
    var name: String
    var email: String?
    var subscribe: [Truck] = []
    
    init(name: String){
        //self.id = id
        self.name = name
        //self.email = email
        //self.subscribe = subscribe
    }
    
    // return # of subscribe
    func countOfSubscriber() -> Int {
        return self.subscribe.count
    }
    
}