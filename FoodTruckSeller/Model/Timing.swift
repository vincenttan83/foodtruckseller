//
//  Timing.swift
//  FoodTruckSeller
//
//  Created by Vincent Tan on 28/11/2015.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import Foundation

class Timing {
    
    var id: Int?
    var descriptions: String?
    
    init (id: Int, descriptions: String) {
        self.id = id
        self.descriptions = descriptions
    }
    
}