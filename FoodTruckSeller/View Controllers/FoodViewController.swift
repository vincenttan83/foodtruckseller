//
//  FoodViewController.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/28/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import UIKit

class FoodViewController: UIViewController {

    @IBOutlet weak var foodTableView: UITableView!
    
    let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    var foods : [Food] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        self.preloadData()
//        print(foods)
        self.refreshParks()
    }
    
    func preloadData(){
        preloadDataFromFileSystem()
//        preloadDataNSDate()
//
//                self.foodTableView.reloadData()
    }
    
    func preloadDataFromFileSystem(){
        self.foods = FoodLoader.sharedLoader.readFoodsFromFile()
    }
    
    func preloadDataNSDate(){
        if let dataArray: [NSData] = NSUserDefaults.standardUserDefaults().objectForKey("foodDataArray") as? [NSData] {
            for data in dataArray {
                if let food = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? Food {
                                self.foods.append(food)
                    }
                }
            }
    }
    
    func refreshParks() {
        FoodLoader.sharedLoader.readFoodsFromServer { (foods, error) -> Void in
            if let error = error {
                
            } else {
                self.foods = foods
                self.foodTableView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showFoodDetailViewController" {
            
            let destVC: FoodItemDetailViewController = segue.destinationViewController as! FoodItemDetailViewController
            
            if let selectedIndexPath = self.foodTableView.indexPathForSelectedRow {
                let food: Food = self.foods[selectedIndexPath.row]
                destVC.food = food
                
            }
        } else if segue.identifier == "showAddParkViewController" {
            
            //completionBlock
            let destVC: AddFoodViewController = segue.destinationViewController as! AddFoodViewController
            
            destVC.completionBlock = { (food: Food!) -> Void in }
        }
    }
    
    //unwind
    @IBAction func unwindToFoodListViewController(segue: UIStoryboardSegue) {
        if let addFoodVC: AddFoodViewController = segue.sourceViewController as? AddFoodViewController{
            if let food = addFoodVC.food {
                self.foods.append(food)
                
                // Save food to user defaults
//                self.saveFoodToUserDefaults(food)
                
                //Save food to filesystem
//                self.saveFoodToFileSystem(self.foods)
                
                //Save food to API
                self.saveFoodToApi(food)
                
                self.foodTableView.reloadData()
            }
        }
    }
    
    func saveFoodToApi(food: Food){
        FoodLoader.sharedLoader.createFoodOnServer(food) { (succeeded, error) -> Void in
        }
    }
    
    // Save food to user defaults
    func saveFoodToUserDefaults(foods: Food){
        
        var foodDataArray: [NSData] = []
        for food in self.foods {
            let foodData: NSData = NSKeyedArchiver.archivedDataWithRootObject(food)
            foodDataArray.append(foodData)
        }
        
//        NSUserDefaults.standardUserDefaults().setObject(foodDataArray, forKey: "foodDataArray")
//        NSUserDefaults.standardUserDefaults().synchronize()
    
    }
    
    func saveFoodToFileSystem(foods: [Food]){
        
        FoodLoader.sharedLoader.saveFoodsToFile(foods)
        
    }

}

extension FoodViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foods.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: FoodItemTableViewCell = self.foodTableView.dequeueReusableCellWithIdentifier("FoodCell", forIndexPath: indexPath) as! FoodItemTableViewCell
        
        let food : Food = self.foods[indexPath.row]
        cell.food = food
        
        return cell
        
    }

}

extension FoodViewController : UITableViewDelegate {
    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        let detailsVC: FoodItemDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FoodItemDetailViewController") as! FoodItemDetailViewController
//        
//        let food: Food = self.foods[indexPath.row]
//        detailsVC.food = food
//        
//        self.navigationController?.pushViewController(detailsVC, animated: true)
//    }

}
