//
//  FoodItemTableViewCell.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/28/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

//U+2606

import UIKit

class FoodItemTableViewCell: UITableViewCell {

    @IBOutlet weak var labelPrice: UILabel!

    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelStars: UILabel!
    
    @IBOutlet weak var foodImage: UIImageView!
    
    var food : Food!{
        didSet{
            updateDisplay()
        }
    }
    
    func updateDisplay(){
        self.labelPrice.text = "\(self.food.price!)"
        self.labelName.text = self.food.name
//        self.labelStars.text = "\(self.food.averageFoodItemRating())"
        self.foodImage.image = self.food.photo
    }
    

}
