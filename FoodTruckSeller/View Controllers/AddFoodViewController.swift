//
//  AddFoodViewController.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/28/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import UIKit

//DelegatingProtocol
protocol AddFoodDelegate {
    func addFoodViewController(AddFoodViewController: AddFoodViewController, didAddFood: Food!)
}

class AddFoodViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var foodNameTextField: UITextField!
    @IBOutlet weak var foodPriceTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    //unwind
    var food : Food?
    
    //completionBlock
    var completionBlock: ((food: Food!)->Void)?
    
    //Delegate
    var addFoodDelegate: AddFoodDelegate?
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        foodNameTextField.becomeFirstResponder()
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //unwind
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "unwindToFoodListViewController" {
            self.food = Food(name: foodNameTextField.text!, price: Double(foodPriceTextField.text!))!
            if imageView.image != nil {
                self.food!.photo = imageView.image!
            }
        }
    }
    
    @IBAction func imagePicker(sender: UIButton) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            imageView.contentMode = .ScaleAspectFit
            imageView.image = possibleImage
        } else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            imageView.image = possibleImage
        } else {
            return
        }
//        NSLog("File path: \(imageView.image!)")
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //completionBlock
    @IBAction func addFood(sender: AnyObject) {
        let food: Food = Food(name: foodNameTextField.text!, price: Double(foodPriceTextField.text!))!
        
        //delegate
        self.addFoodDelegate?.addFoodViewController(self, didAddFood: food)
        
        //completionBlock
//        self.completionBlock?(food: food)
    }

    @IBAction func cancel(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
