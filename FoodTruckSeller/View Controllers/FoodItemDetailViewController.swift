//
//  FoodItemDetailViewController.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/28/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import UIKit

class FoodItemDetailViewController: UIViewController {
    
    var food : Food!
    
    @IBOutlet weak var foodImage: UIImageView!

    
    @IBOutlet weak var foodNameLabel: UILabel!
    
    
    @IBOutlet weak var foodNamePriceLabel: UILabel!
    
    @IBOutlet weak var foodStarsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.foodNameLabel.text = self.food.name
        self.foodNamePriceLabel.text = "\(self.food.price!)"
        self.foodStarsLabel.text = "\(self.food.averageFoodItemRating())"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
