//
//  UserClassTests.swift
//  FoodTruckSeller
//
//  Created by Vincent Tan on 21/11/2015.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import XCTest
@testable import FoodTruckSeller

class UserClassTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUserInitialization() {
        let testUser: User = User(name: "Test username")
        XCTAssertNotNil(testUser, "Test truck should not be nil")
    }
    
    func testUserSubscribeToTruck() {
        let testUser: User = User(name: "Tester")
        
        let firstTruck: Truck = Truck(name: "first truck")
        let secondTruck: Truck = Truck(name: "second truck")
        
        testUser.subscribe.append(firstTruck)
        testUser.subscribe.append(secondTruck)
        
        XCTAssertNotNil(testUser.subscribe, "User subscribe should not be nil")
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
