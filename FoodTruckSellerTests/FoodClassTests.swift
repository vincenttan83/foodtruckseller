//
//  FoodClassTests.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/21/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import XCTest
@testable import FoodTruckSeller

class FoodClassTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testFoodItemIntialization() {
        let foodItem: Food? = Food(name: "Jumbo Burger", price: 12.00)
        XCTAssertNotNil(foodItem, "Food Item should not be nil")
        
        let unamedFoodItem: Food? = Food(name: "", price: 13.00)
        XCTAssertNil(unamedFoodItem, "Food Item name should not be nil")
        
        let unpricedFoodItem: Food? = Food(name: "Fries", price: nil)
        XCTAssertNil(unpricedFoodItem, "Food Item price should not be nil")
        
        let unamedAndUnpricedFoodItem: Food? = Food(name: "", price: nil)
        XCTAssertNil(unamedAndUnpricedFoodItem, "Food Item name & price should not be nil")
        
    }
    
    func testAddRating(){
        let nasiLemak: Food? = Food(name: "Nasi Lemak", price: 15.00)
        
        if let firstRating = Rating(rating: 5.0, comment: "banyak sedap", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle"){
            nasiLemak!.ratings.append(firstRating)
        }
        
        if let secondRating = Rating(rating: 5.0, comment: "banyak sedap", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle"){
            nasiLemak!.ratings.append(secondRating)
        }
        
        XCTAssertTrue(nasiLemak!.ratings.count == 2, "reviews count should match 2")
        
        let averageRating = nasiLemak!.averageFoodItemRating()
        let expectedRatingValue: Double = 5.0
        
        XCTAssertEqual(averageRating, expectedRatingValue, "averageFoodItemRating should match expected average of 5.0")
        
        _ = nasiLemak!.addRating(5.0, comment: "super nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")
        
        XCTAssertTrue(nasiLemak!.ratings.count == 3, "reviews count should match 3")
 
    }
    
    
    func testSortFoodMenuItem(){
        let testRating: Food = Food(name: "Nasi Lemak", price: 15.00)!
        
        
        let firstRating: Rating = Rating(rating: 3, comment: "very nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")!
        let secondRating: Rating = Rating(rating: 5, comment: "very nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")!
        let thirdRating: Rating = Rating(rating: 2, comment: "very nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")!
        
        testRating.ratings.append(firstRating)
        testRating.ratings.append(secondRating)
        testRating.ratings.append(thirdRating)
        
        let sortedRating: [Rating] = testRating.sortedRating()
        let expectedRatingOrder: [Double] = [2, 3, 5]
        
        
        for i in 0 ..< sortedRating.count {
            XCTAssertTrue(sortedRating[i].rating! == expectedRatingOrder[i])
        }
        
    }
    
}