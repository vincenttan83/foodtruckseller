//
//  FoodPersistenceTests.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 12/2/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import XCTest
@testable import FoodTruckSeller

class FoodPersistenceTests: XCTestCase {
    
    var session: NSURLSession!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        setUpForReadWriteToFile()
        setUpForReadWriteToApi()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func setUpForReadWriteToFile(){
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if defaults.objectForKey("SavedFood1") == nil {
            let foods: [String] = ["Nasi Lemak", "Mee Goreng", "Hot Dog"]
            defaults.setObject(foods, forKey: "SavedFood1")
            defaults.synchronize()
        }
    
    }
    
    func setUpForReadWriteToApi(){
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = ["X-Parse-Application-Id": "tCUyLTP0oZI3TTfr2MIA2GCkaOP7bFAor44oSD0U", "X-Parse-REST-API-Key": "0g1vSqt0OAhOm3mDUnTj1Hy94mrOSyihSLZNsJf5", "Content-Type": "application/json"]
        self.session = NSURLSession(configuration: config)
    }
    
    func testWritingToFile(){
//        let fileManager: NSFileManager = NSFileManager.defaultManager()
//        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
//        
//        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        var foods: [Food] = []
        foods.append(Food.createRandomFood())
        foods.append(Food.createRandomFood())
        foods.append(Food.createRandomFood())
        
        FoodLoader.sharedLoader.saveFoodsToFile(foods)
        
        let filePath: NSURL = FoodLoader.sharedLoader.dataFileURL()
        
        let fileExists: Bool = NSFileManager.defaultManager().fileExistsAtPath(filePath.path!)
        
//        var foodsData: [NSData] = []
//        for food in foods {
//            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(food)
//            foodsData.append(data)
//        }
        
//        (foodsData as NSArray).writeToURL(filePath, atomically: true)
        
//        let fileExists: Bool = fileManager.fileExistsAtPath(filePath.path!)
        XCTAssert(fileExists, "File should exists after write")
        
        NSLog("File path: \(filePath.path!)")
        
    }
    
    func testReadingFromFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        let foodsData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        var foods: [Food] = []
        
        for data in foodsData {
            let food: Food = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Food
            foods.append(food)
        }
        
        XCTAssert(foods.count == 3, "Should have loaded 3 parks from file")
    }

    
    func testSavingFood(){
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        let food: Food = Food.createRandomFood()
        let foodData: NSData = NSKeyedArchiver.archivedDataWithRootObject(food)
        
        defaults.setObject(foodData, forKey: "testFood")
        defaults.synchronize()
        
        let readFoodData: NSData = defaults.objectForKey("testFood") as! NSData
        let readFood: Food? = NSKeyedUnarchiver.unarchiveObjectWithData(readFoodData) as? Food
        
        XCTAssertNotNil(readFood, "Read back from saved data")
        XCTAssertEqual(readFood!.name, food.name, "Names should match")
        
    }
    
    func testReadingArray(){
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let foods = defaults.objectForKey("SavedFood1") as? [String] {
            XCTAssert(foods.count > 0, "Saved list of foods should contain some items")
        } else {
            XCTAssert(false, "Unable to read list of foods")
        }
    }
    
    func testReadingFromDefaults() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject("Nasi Lemak", forKey: "SavedFood")
        defaults.synchronize()
        
        let name: String? = defaults.stringForKey("SavedFood")
        XCTAssertEqual("Nasi Lemak", name, "Saved name should match")
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLoadingAPI() {
        let expectation = self.expectationWithDescription("Load Task")
        
        let url: NSURL = NSURL(string: "https://api.parse.com/1/classes/Food")!
        
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
            //            let htmlString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            //            XCTAssert(htmlString != nil, "Should have received some html String from server")
            
            
            //NSLog(htmlString!)
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Server should have returned 200 Content OK")
            
            
            var foods = [Food]()

            let json : [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            
            if let resultsArray: [ [String : AnyObject] ] = json["results"] as? [ [String : AnyObject] ] {
                for jsonData in resultsArray {
                    let foodName: String = jsonData["foodName"] as! String
                    // let objectId: String = jsonData["objectId"] as! String
                    
                    foods.append(
                        Food(
                            name: foodName,
                            price: (jsonData["price"] as! Double)
                        )!
                    )
                }
            }
            
            XCTAssert(foods.count > 0, "Foods array shouldn't be empty")
            expectation.fulfill()
        }
        
        loadTask.resume()
        self.waitForExpectationsWithTimeout(100, handler: nil)
    }
    
}
