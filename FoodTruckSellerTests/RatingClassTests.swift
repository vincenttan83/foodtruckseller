//
//  RatingClassTests.swift
//  FoodTruckSeller
//
//  Created by Esu Chang on 11/21/15.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import XCTest
@testable import FoodTruckSeller

class RatingClassTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testRatingIntialization() {
        
        let actualRating: Double? = 10.0
        let expectedRating: Double? = 5.0
        
        let testRating: Rating? = Rating(rating: actualRating, comment: "very nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")
        XCTAssertNotNil(testRating, "Rating should not be nil")
        
        let nilRatingAndNoCommentRating: Rating? = Rating(rating: nil, comment: "", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "")
        XCTAssertNil(nilRatingAndNoCommentRating, "Rating & comment & createdBy should not be nil")
        
        let testEverythingIsNilRating: Rating? = Rating(rating: nil, comment: "", dateCreated: nil, dateUpdated: nil, createdBy: "")
        XCTAssertNil(testEverythingIsNilRating, "Rating & comment & createdBy & dateUpdated & dateCreated should not be nil")
        
        let noCommentRating: Rating? = Rating(rating: actualRating, comment: "", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")
        XCTAssertNil(noCommentRating, "Comment should not be nil")
        
        let testNilRating: Rating? = Rating(rating: nil, comment: "very nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")
        XCTAssertNil(testNilRating, "Rating & comment should not be nil")
        
        let nilCreatedByRating: Rating? = Rating(rating: actualRating, comment: "very nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "")
        XCTAssertNil(nilCreatedByRating, "Created by should not be nil")
        
        let testDateCreatedAndDateUpdatedRating: Rating? = Rating(rating: actualRating, comment: "very nice", dateCreated: nil, dateUpdated: nil, createdBy:  "Estelle")
        XCTAssertNil(testDateCreatedAndDateUpdatedRating, "testDateCreatedAndDateUpdatedRating should be nil")
        
        let testRatingMoreThanFive: Rating = Rating(rating: actualRating, comment: "very nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")!
        XCTAssert(testRatingMoreThanFive.rating == expectedRating)
        
        
        let actualRatingLessThanOne: Double? = 0
        let expectedRatingLessThanOne: Double? = 1.0
        
        let testRatingLessThanOne: Rating = Rating(rating: actualRatingLessThanOne, comment: "very nice", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Estelle")!
        XCTAssert(testRatingLessThanOne.rating == expectedRatingLessThanOne)
        
    }
    
//    func testRating
    
}
