//
//  TruckClassTests.swift
//  FoodTruckSeller
//
//  Created by Vincent Tan on 21/11/2015.
//  Copyright © 2015 vincenttan. All rights reserved.
//

import XCTest
@testable import FoodTruckSeller

class TruckClassTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTruckInitialization() {
        let testTruck: Truck = Truck(name: "Test name")
        XCTAssertNotNil(testTruck, "Test truck should not be nil")
    }
    
    func testAddingReviewToTruck() {
        let testTruck: Truck = Truck(name: "Test Truck")
        
        let firstReview: Rating = Rating(rating: 3, comment: "good and cheap", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Jason")!
        let secondReview: Rating = Rating(rating: 5, comment: "cheap and good", dateCreated: NSDate(), dateUpdated: NSDate(), createdBy: "Jeremy")!
        
        testTruck.ratings.append(firstReview)
        testTruck.ratings.append(secondReview)
        
        let averageRating = testTruck.AverageRating()
        let expectedRating: Double = 4.0
        
        XCTAssertNotNil(testTruck.ratings, "Test rating to truck should not be nill")
        XCTAssertEqual(averageRating, expectedRating, "Average rating should be match expected of 4.0")
        
    }

    func testAddRating() {
        let testTruck: Truck = Truck(name: "Test Truck")
        
        let addRating = testTruck.addRating(1, comment: "test comment", commentor: "Jason")
        
        
        
    }
    
    
    func testSortFoodMenuItem(){
        let testTruck: Truck = Truck(name: "Test Truck")
        
        let nasiLemak: Food = Food(name: "nasiLemak", price: 1.00)!
        let meeGoreng: Food = Food(name: "meeGoreng", price: 5.00)!
        let chickenRice: Food = Food(name: "chickenRice", price: 5.00)!
        
        testTruck.foods.append(nasiLemak)
        testTruck.foods.append(meeGoreng)
        testTruck.foods.append(chickenRice)
        
        let sortedFood: [Food] = testTruck.sortedFoodByName()
        let expectedFoodNameOrder: [String] = ["chickenRice", "meeGoreng", "nasiLemak"]
        
        
        for i in 0 ..< sortedFood.count {
            XCTAssertTrue(sortedFood[i].name! == expectedFoodNameOrder[i])
        }
    
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
